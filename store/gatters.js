const getters = {
	id: state => state.user.id,
	avatar: state => state.user.avatar,
	name: state => state.user.name,
	num: state => state.user.num,
	email: state => state.user.email,
	numName: state => state.user.num + '' + state.user.name,
	role: state => state.user.role
}

export default getters;