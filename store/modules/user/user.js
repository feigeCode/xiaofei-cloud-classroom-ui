const user = {
	state: {
		id: '',
		avatar: '',
		num: '',
		name: '',
		email: '',
		role: ''
	},
	mutations: {
		SET_ID: (state, id) => {
			state.id = id
		},
		SET_NUM: (state, num) => {
			state.num = num
		},
		SET_NAME: (state, name) => {
			state.name = name
		},
		SET_EMAIL: (state, email) => {
			state.email = email
		},
		SET_AVATAR: (state, avatar) => {
			state.avatar = avatar
		},
		SET_ROLE: (state, role) => {
			state.role = role
		}
	}
}

export default user;