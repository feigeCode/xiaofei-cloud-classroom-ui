import Vue from 'vue'
import App from './App'
import Uview from 'uview-ui'
import store from './store/index.js'
import { createSocket } from "@/common/xiaofei-im/xiaofei-im-js-sdk";
import { isEmpty } from '@/common/xiaofei-im/xiaofei-im-utils';
import { getToken } from "@/common/auth";
import { getNettyUrl } from "@/utils/constants"
import { timestampFormat, formatter } from "@/utils/date"

Vue.config.productionTip = false
Vue.use(Uview);
const token = getToken();
if(!isEmpty(token)){
    Vue.prototype.$fim = createSocket(getNettyUrl(), {
        token: token,
        language: "zh-CN",
        address: "四川省成都市",
        ip: "127.0.0.1",
    });
}
App.mpType = 'app'

Vue.filter("dateFormat", timestampFormat);
Vue.filter("formatter", formatter);
const app = new Vue({
    ...App,
    store
})
// http拦截器，将此部分放在new Vue()和app.$mount()之间，才能App.vue中正常使用
import httpInterceptor from '@/common/http.interceptor.js'
Vue.use(httpInterceptor, app)

// http接口API抽离，免于写url或者一些固定的参数
import httpApi from '@/common/api/http.api.js'
// import httpApi from '@/common/api/http.cloud.api.js'
Vue.use(httpApi, app)

app.$mount()
