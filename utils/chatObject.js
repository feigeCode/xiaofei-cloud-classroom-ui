export function PrivateChatMsg(senderId,receiverId,type,content,extend) {
	this.senderId = senderId;
	this.receiverId = receiverId;
	this.type = type;
	this.content = content;
	this.extend = extend;
	
}

export function GroupChatMsg(senderId,groupId,type,content,extend) {
	this.senderId = senderId;
	this.groupId = groupId;
	this.type = type;
	this.content = content;
	this.extend = extend;

}


export function DataContent(type,chatMsg,extend) {
	this.type = type;
	this.chatMsg = chatMsg;
	this.extend = extend;
}




/**
 * 聊天快照对象
 * @param {Object} uid
 * @param {Object} msg
 * @param {Object} count
 * @param {Object} gmtCreate
 * @param {Object} type
 * @param {Object} stick
 */
export function ChatSnapshot(uid, msg, count, gmtCreate, type,stick) {
	this.uid = uid;
	this.msg = msg;
	this.count = count;
	this.gmtCreate = gmtCreate;
	this.type = type;
	this.stick = stick;
}