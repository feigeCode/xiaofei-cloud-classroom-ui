import {ChatSnapshot} from "./chatObject";

const CHAT_SNAPSHOT = "chat-snapshot";
const CHAT_MSG = "chat-msg"
export function setChatSnapshotCount(myUid,uid) {
	let data = getChatSnapshot(myUid);
	if (data != null){
		for(let i = 0;i < data.length;i++){
			if(data[i].uid === uid && data[i].count > 0){
				data[i].count = 0;
				uni.setStorage({
					key: CHAT_SNAPSHOT + myUid,
					data: JSON.stringify(data),
					success: function () {
						uni.$emit("refreshUserChatSnapshotCount", uid)
						console.log('set ChatSnapshotCount success');
					}
				});
				break;
			}
		}
	}

}
export function setChatSnapshot(myUid,chatSnapshot) {
	let data = getChatSnapshot(myUid);
	let count = chatSnapshot.count;
	let stick = false;
	if(data !== null){
		for(let i = 0;i < data.length;i++){
			if(data[i].uid === chatSnapshot.uid){
				count += data[i].count;
				stick = data[i].stick;
				data.splice(i,1);
				break;
			}
		}
	}else{
		data = [];
	}
	chatSnapshot.count = count;
	chatSnapshot.stick = stick;
	data.unshift(chatSnapshot);
	uni.setStorage({
		key: CHAT_SNAPSHOT + myUid,
		data: JSON.stringify(data),
		success: function () {
			console.log(' set ChatSnapshot success');
		}
	});
}

export function getChatSnapshot(myUid) {
	try {
		const value = uni.getStorageSync(CHAT_SNAPSHOT + myUid);
		if (value) {
			return JSON.parse(value);
		}
	} catch (e) {
		console.log(e);
	}
	return null;
}

export function getChatSnapshotByUserId(myUid,uid) {
	let data = getChatSnapshot(myUid);
	if(data !== null){
		for(let i = 0;i < data.length;i++){
			if(data[i].uid === uid){
				data[i].i = i;
				return data[i];
			}
		}
	}
	return null;
}

export function removeChatSnapshot(myUid) {
	uni.removeStorage({
		key: CHAT_SNAPSHOT + myUid,
		success: function (res) {
			console.log('remove ChatSnapshot success');
		}
	});
}


export function setChatMsg(myUid,uid,chatMsg) {
	let msg = getChatMsg(myUid,uid);
	if (msg == null){
		msg = [];
	}
	msg.push(chatMsg);
	if (msg.length > 20){
		msg.splice(0,msg.length - 20);
	}
	uni.setStorage({
		key: myUid + CHAT_MSG + uid,
		data: JSON.stringify(msg),
		success: function () {
			console.log(' set ChatMsg success');
		}
	});
}

export function getChatMsg(myUid,uid) {
	try {
		const value = uni.getStorageSync(myUid + CHAT_MSG + uid);
		if (value) {
			return JSON.parse(value);
		}
	} catch (e) {
		console.log(e);
	}
	return null;
}


export function removeChatMsg(myUid,uid) {
	uni.removeStorage({
		key: myUid + CHAT_MSG + uid,
		success: function (res) {
			console.log('remove ChatMsg success');
		}
	});
}

/**
 * 快照消息构造器
 * @param msg
 * @returns {[{children: [{text: string, type: string}], name: string}]}
 */
export function chatSnapshotBuilder(msg){
	// 如果消息为语音或图片则聊天快照不存储
	let snapMsg = [{
		children: [
			{
				type: "text",
				text: "[暂无消息]"
			}
		],
		name: "div"
	}];
	if (msg.type <= 2) {
		snapMsg = msg.content;
	} else if (msg.type === 3) {
		snapMsg[0].children[0].text = "[语音]";
	} else if (msg.type === 4) {
		snapMsg[0].children[0].text = "[图片]";
	}
	return snapMsg;
}

/**
 * 消息构造器
 * @param msg
 * @returns {ChatSnapshot}
 */
export function chatMsgBuilder(msg){
	if(msg.type === 3){
		msg.length = msg.extend;
	}else if(msg.type === 4){
		let w = msg.extend.split(":")[0];
		let h = msg.extend.split(":")[1];
		msg.w = w;
		msg.h = h;
	}else {
		msg.content = JSON.parse(msg.content);
	}
	return new ChatSnapshot(msg.uid,chatSnapshotBuilder(msg),1,msg.gmtCreate,msg.type,false);
}


export function chatMsgHandler(myUid,msg){
	let chatSnapshot = chatMsgBuilder(msg);
	uni.$emit("refreshUserChatSnapshot",chatSnapshot);
	setChatMsg(myUid,msg.uid,msg);
	setChatSnapshot(myUid,chatSnapshot);
}

export function networkStatus(){
	uni.getNetworkType({
		success: function (res) {
			if (res.networkType === 'none') {
			  uni.showToast({
					icon: 'loading',
					title: "哎呀，网络丢失了！"
				})
			}
		}
	});
}





