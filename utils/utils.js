/**
 * 字符串是否含有html标签的检测
 * @param htmlStr
 */
 export function isHtml(htmlStr) {
  var  reg = /<[^>]+>/g;
  return reg.test(htmlStr);
}