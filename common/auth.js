//const TOKEN_KEY = 'Authorization';
const TOKEN_KEY = 'Authorization';
const USER_INFO_KEY = "userInfo";

export function setToken(token) {
	uni.setStorage({
		key: TOKEN_KEY,
		//data: "Bearer" + token,
		data: token,
		success: function () {
			console.log(' set token success');
		}
	});
}

export function getToken() {
	try {
		const value = uni.getStorageSync(TOKEN_KEY);
		if (value) {
			return value;
		}
	} catch (e) {
		console.log(e);
	}
	return null;
}


export function removeToken() {
	uni.removeStorage({
		key: TOKEN_KEY,
		success: function (res) {
			console.log('remove token success');
		}
	});
}

export function setUserInfo(userInfo) {
	uni.setStorage({
		key: USER_INFO_KEY,
		data: JSON.stringify(userInfo),
		success: function () {
			console.log(' set userInfo success');
		}
	});
}

export function getUserInfo() {
	try {
		const value = uni.getStorageSync(USER_INFO_KEY);
		if (value) {
			return JSON.parse(value);
		}
	} catch (e) {
		console.log(e);
	}
	return null;
}


export function removeUserInfo() {
	uni.removeStorage({
		key: USER_INFO_KEY,
		success: function (res) {
			console.log('remove userInfo success');
		}
	});
}