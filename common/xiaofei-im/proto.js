const proto = {
  com: {
    feige: {
      im: {
        pojo: {
          proto: {
            AckMsg: {
              MsgType: {

              },
              Status: {

              }
            },
            Auth: {

            },
            Msg: {

            },
            NotifyMsg: {

            },
            TransportMsg: {
              MsgType: {

              }
            },
            Forced: {

            },
            Ping: {

            },
            Pong: {

            }
          }
        }
      }
    }
  }
};

module.exports = proto
