import "./DefaultMsg_pb";
import { DEFAULT_MSG_PACKAGE_PREFIX } from './xiaofei-im-utils'
/**
 * 私聊消息VO
 */
export class ChatMsgVo {
  constructor(id, friendRelationId, senderId, receiverId, type, content, extra, status, format, gmtCreate) {
    this.id = id;
    this.friendRelationId = friendRelationId;
    this.senderId = senderId;
    this.receiverId = receiverId;
    this.type = type;
    this.content = content;
    this.extra = extra;
    this.status = status;
    this.format = format;
    this.gmtCreate = gmtCreate;
  }
  
  toPriTransportMsgPb() {
    const msg = new DEFAULT_MSG_PACKAGE_PREFIX.Msg()
      .setId(this.id)
      .setFriendrelationid(this.friendRelationId)
      .setSenderid(this.senderId)
      .setReceiverid(this.receiverId)
      .setMsgtype(this.type)
      .setContent(this.content)
      .setExtra(this.extra)
      .setStatus(this.status)
      .setFormat(this.format)
      .setGmtcreate(this.gmtCreate);
    return new DEFAULT_MSG_PACKAGE_PREFIX.TransportMsg()
      .setType(DEFAULT_MSG_PACKAGE_PREFIX.TransportMsg.MsgType.PRIVATE)
      .setMsg(msg);
  }


  toGroupTransportMsgPb() {
    const msg = new DEFAULT_MSG_PACKAGE_PREFIX.Msg()
      .setId(this.id)
      .setFriendrelationid(this.friendRelationId)
      .setSenderid(this.senderId)
      .setReceiverid(this.receiverId)
      .setMsgtype(this.type)
      .setContent(this.content)
      .setExtra(this.extra)
      .setStatus(this.status)
      .setFormat(this.format)
      .setGmtcreate(this.gmtCreate);
    return new DEFAULT_MSG_PACKAGE_PREFIX.TransportMsg()
      .setType(DEFAULT_MSG_PACKAGE_PREFIX.TransportMsg.MsgType.GROUP)
      .setMsg(msg);
  }
}

/**
 * 消息类型
 */
export const TEXT_TYPE = 1;
export const AUDIO_TYPE = 2;
export const IMG_TYPE = 3;
export const HTML_TYPE = 4;

/**
 * 消息序列化类型
 */
export const HTML_FORMAT = 1;
export const XML_FORMAT = 2;
export const PROBUF_FORMAT = 3;