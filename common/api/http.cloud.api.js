const ModuleName = {
	AUTH: '/a',
	COURSE: '/c',
	SIGN: '/s',
	MSG: '/m',
	USER: '/u',
	FILE: '/f',
	EXERCISE: '/e'
}

const file = ModuleName.FILE + "/file"

const course = ModuleName.COURSE + "/course"
const clazz = ModuleName.COURSE + "/class-user"
const swiper = ModuleName.COURSE + "/swiper";
const userCourse = ModuleName.COURSE + "/user-course"
const catalog = ModuleName.COURSE + "/course-catalog"
const courseFile = ModuleName.COURSE + "/course-file"


const privateChatMsg = ModuleName.MSG + "/pri-chat-msg"
const groupChatMsg= ModuleName.MSG + "/group-chat-msg";
const notifyMsg = ModuleName.MSG + "/notify-msg"


const askForLeave = ModuleName.SIGN + "/ask-for-leave"
const signTask = ModuleName.SIGN + "/sign-task"
const signIn =  ModuleName.SIGN + "/sign-in"


const user = ModuleName.USER + "/user"
const collect = ModuleName.USER + "/collect";


const exerciseTask =  ModuleName.EXERCISE + "/exercise-task"
const exercise =  ModuleName.EXERCISE + "/exercise"
const userAnswer =  ModuleName.EXERCISE + '/user-answer';
const fraction = ModuleName.EXERCISE + "/fraction";

const index = ModuleName.AUTH + "/index"
const verify = ModuleName.AUTH + "/verify"


const httpApi = (Vue, vm) => {


	
	// 注册相关api
	const senderEmailCode = (email) => vm.$u.get(index + "/sender/" + email)
	const validateEmail = (email, code) => vm.$u.get(index + "/validate/" + email + "/" + code)
	const register = (data) => vm.$u.post(index + "/register", data);
	
	
	// 登录相关api
	const login = (data) => vm.$u.post(index + "/login", data);
	const logout = () => vm.$u.get(index + '/logout');
	const getCodeImg = () => vm.$u.get(verify + '/captcha_image');
	const isShowVerifyCode = (username) => vm.$u.get(`${index}/show/captcha/${username}`)


	//请假相关api
	const getAskForLeaveById = (params) => vm.$u.get(askForLeave + '/get', params);
	const getAskForLeaveByClazzId = (clazzId, params) => vm.$u.get(askForLeave + '/audit/' + clazzId, params);
	const saveAskForLeave = (data) => vm.$u.post(askForLeave + "/save", data);
	const getStuAskForLeaveDetailById = (id) => vm.$u.get(askForLeave + '/stu/detail/' + id);
	const getTeaAskForLeaveDetailById = (id) => vm.$u.get(askForLeave + '/tea/detail/' + id);
	const updateAskForLeave = (data) => vm.$u.put(askForLeave + "/update", data);
	const getNotLeaveOffByUserId = () => vm.$u.get(askForLeave + "/leave-off");

	//课程相关api
	const getMyTeachCourse = (params) => vm.$u.get(`${course}/get/teach`, params);
	const getMyAttendCourse = (params) => vm.$u.get(`${course}/get/attend`, params);
	const createCourse = (data) => vm.$u.post(course + "/save", data);
	const getCourseById = (id) => vm.$u.get(course + "/get/" + id);
	const getCourseByInvestCode = (investCode) => vm.$u.get(course + "/get/invest/" + investCode);
	const getUserByCourseId = (courseId) => vm.$u.get(course + "/create/user/" + courseId);
	const deleteCourseOrClazz = (type, id) => vm.$u.delete(course + "/delete/" + id +"/" + type);

	// 目录相关api
	const getCatalogByCourseId = (courseId, params) => vm.$u.get(catalog + "/catalogs/" + courseId, params);
	const createCatalog = (data) => vm.$u.post(catalog + "/save", data);
	const getCatalogById = (id) => vm.$u.get(catalog + "/" + id);
	const deleteBatchCatalog = (cid, ids) => vm.$u.delete(catalog + "/delete/" + cid, ids);

	// 班级相关api
	const getClazzVoById = (id) => vm.$u.get(clazz + "/clazz/" + id);
	const getCreateOrJoinClass = () => vm.$u.get(course + "/class");
	const getClazzByInvestCode = (investCode) => vm.$u.get(clazz + "/invest/" + investCode);
	const getUserByClazzId = (clazzId) => vm.$u.get(clazz + "/create/user/" + clazzId);
	const deleteClazz = (clazzId) => vm.$u.delete(clazz + "/delete/" + clazzId);

	// 资料相关api
	const getCourseFilePage = (courseId, params) => vm.$u.get(courseFile + "/get/" + courseId, params);
	const getCourseFile = (id) => vm.$u.get(courseFile + "/" + id);
	const createCourseFile = (data) => vm.$u.post(courseFile + "/save", data);
	const deleteCourseFile = (courseFileId) => vm.$u.delete(courseFile + "/delete/" + courseFileId);

	// 用户相关api
	const getUserOne = (email) => vm.$u.get(user + "/get/" + email)
	const getFriendUserByFriendRelationId = (friendRelationId) => vm.$u.get(`${user}/get/friend-user/${friendRelationId}`)
	const updateUser = (data) => vm.$u.put(user + "/update", data);
	const addFriend = (id) => vm.$u.get(`${user}/add-friend/${id}`);
	const deleteFriend = (id) => vm.$u.get(`${user}/delete-friend/${id}`);
	const joinClazz = (userId, clazzId) => vm.$u.put(user + "/join/" + userId + "/" + clazzId);
	const getStudentByClazzId = (pageNum, clazzId) => vm.$u.get(user + "/get/student/" + pageNum + "/" + clazzId);
	const changePwd = (data) => vm.$u.put(user + "/change/pwd", data);
	const forgetPwd = (data) => vm.$u.put(user + "/forget/pwd", data);
	const exitClazz = (userId,clazzId) => vm.$u.put(user + "/exit/" + userId + "/" + clazzId);
  const getInfo = () => vm.$u.get(user + '/info');

	// 用户课程相关api
	const getCourseUser = (courseId, pageNum) => vm.$u.get(userCourse + "/" + pageNum + "/" + courseId);
	// 班级或课程 type = 1课程 type = 2班级
	const getMemberList = (type, courseId, params) => vm.$u.get(`${userCourse}/members/${type}/${courseId}`, params);
	const getAuditUserVos = (type, courseId, params) => vm.$u.get(`${userCourse}/audit-user/${type}/${courseId}`, params);
	const auditBatch = (type, data) => vm.$u.put(`${userCourse}/audit/${type}`, data);
	const joinCourse = (type, id) => vm.$u.post(userCourse + "/join/" + id + "/" + type)
	const kickOut = (userId, courseId) => vm.$u.delete(userCourse + "/delete/" + userId + "/" + courseId);

	// 签到任务相关api
	const postSignTask = (data) => vm.$u.post(signTask + "/save", data);
	const getSignTaskByCourseId = (courseId, params) => vm.$u.get(signTask + "/get/task/" + courseId, params);
	const getSignStatusList = (courseId, params) => vm.$u.get(signTask + "/s/" + courseId, params);

	// 私聊消息相关api
	const queryHistoryChatMsg = (friendRelationId, params) => vm.$u.get(`${privateChatMsg}/history/${friendRelationId}`, params);
	const queryUnsignedMsg = (receiverId) => vm.$u.get(privateChatMsg + "/get/unsigned/" + receiverId);
	const getChatUser = (params) => vm.$u.get(privateChatMsg + "/get/chat-user", params);

	// 群聊消息相关api
	const queryGroupMsg = (groupId, params) => vm.$u.get(groupChatMsg + "/get/" + groupId, params);

	// 通知消息相关api
	const queryNotifyMsg = (params) => vm.$u.get(notifyMsg + "/get", params);
	const updateNotifyMsgUser = (data) => vm.$u.post(notifyMsg + "/update", data);


	// 签到相关api
	const saveSignIn = (data) => vm.$u.post(signIn + "/save", data);
	const getSignDetail = (signTaskId) => vm.$u.get(signIn + "/detail/" + signTaskId);

	// 上传base64的图片
	const uploadBase64Data = (data) => vm.$u.post(file + "/base64Upload", data);


	// 习题任务相关api
	const saveExerciseTask = (data) => vm.$u.post(exerciseTask + "/save", data);
	const getExerciseTask = (id, params) => vm.$u.get(`${exerciseTask}/teacher/${id}`, params);
	const getStuExerciseTask = (id, params) => vm.$u.get(`${exerciseTask}/student/${id}`, params);

	// 习题相关api
	const saveExercise = (data) => vm.$u.post(exercise + "/save", data);
	const getExercise = (id, params) => vm.$u.get(`${exercise}/stu/${id}`, params);
	const getAnswerExercise = (id, userId, params) => vm.$u.get(`${exercise}/answer/${id}/${userId}`, params);
	const getTeaExercise = (id, params) => vm.$u.get(`${exercise}/tea/${id}`, params);
	const updateExercise = (data) => vm.$u.put(exercise + "/update", data);
	const removeExercise = (id) => vm.$u.delete(exercise + "/delete/" + id);

	// 用户答案api
	const saveUserAnswer = (id, data) => vm.$u.post(userAnswer + "/save/" + id, data);

	// 学生分数
	const getFraction = (id, params) => vm.$u.get(`${fraction}/stu/${id}`, params);

	//收藏相关api
	const getCollect = (params) => vm.$u.get(collect + "/my", params);
	const saveCollect = (data) => vm.$u.post(collect + "/save", data);
	const removeCollect = (id) => vm.$u.delete(collect + "/delete/" + id);

	//轮播图相关api
	const getSwiper = () => vm.$u.get(swiper + "/get");

	// 获取ID
	const getId = () => vm.$u.get(`${ModuleName.MSG}/msg-id/get`);

	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		senderEmailCode,
		register,
		validateEmail,

		login,
		getInfo,
		logout,
		getCodeImg,
		isShowVerifyCode,

		saveAskForLeave,
		getAskForLeaveById,
		getStuAskForLeaveDetailById,
		getTeaAskForLeaveDetailById,
		getAskForLeaveByClazzId,
		updateAskForLeave,
		getNotLeaveOffByUserId,

		getMyTeachCourse,
		getMyAttendCourse,
		createCourse,
		getCourseById,
		getCourseByInvestCode,
		getUserByCourseId,
		deleteCourseOrClazz,


		getCatalogByCourseId,
		createCatalog,
		getCatalogById,
		deleteBatchCatalog,

		getClazzByInvestCode,
		getCreateOrJoinClass,
		getClazzVoById,
		getUserByClazzId,
		deleteClazz,

		getCourseFile,
		getCourseFilePage,
		createCourseFile,
		deleteCourseFile,

		getUserOne,
		getChatUser,
		getFriendUserByFriendRelationId,
		updateUser,
		addFriend,
		deleteFriend,
		joinClazz,
		getStudentByClazzId,
		changePwd,
		forgetPwd,
		exitClazz,

		joinCourse,
		getCourseUser,
		getMemberList,
		getAuditUserVos,
		auditBatch,
		kickOut,

		postSignTask,
		getSignTaskByCourseId,
		getSignStatusList,

		queryHistoryChatMsg,
		queryUnsignedMsg,
		queryGroupMsg,

		queryNotifyMsg,
		updateNotifyMsgUser,

		saveSignIn,
		getSignDetail,

		uploadBase64Data,

		saveExerciseTask,
		getExerciseTask,
		getStuExerciseTask,

		saveExercise,
		getExercise,
		getTeaExercise,
		updateExercise,
		removeExercise,

		getAnswerExercise,
		saveUserAnswer,

		getFraction,

		getCollect,
		saveCollect,
		removeCollect,

		getSwiper,

		getId
	};
}

export default httpApi
